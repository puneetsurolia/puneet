<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   <title>Insert title here</title>
   <script src='https://www.google.com/recaptcha/api.js'></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
   <script src="https://apis.google.com/js/platform.js" async defer></script>
   <meta name="google-signin-client_id" content="371143303432-6ffp8qjm7dbt8u03bn0kb2l3c0orm40m.apps.googleusercontent.com">
</head>
<body>
<center>
   <div class="g-signin2" data-onsuccess="onSignIn" id="myP"></div>
      <img id="myImg"><br>
      <p id="name"></p>
      <div id="status">
   </div>
   <script type="text/javascript">
   
      function onSignIn(googleUser) {
      //window.location.href='static/pages/home.jsp';
      var profile = googleUser.getBasicProfile();
      var imagurl=profile.getImageUrl();
      var name=profile.getName();
      var email=profile.getEmail();
      document.getElementById("myImg").src = imagurl;
      document.getElementById("name").innerHTML = name;
      document.getElementById("myP").style.visibility = "hidden";
      document.getElementById("status").innerHTML = 'Welcome '+name+'!<a href=static/pages/home.jsp?email='+email+'&name='+name+'/>Continue with Google login</a></p>'
   }
   </script>

<script>
FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
});
  
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '1789977611070117',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v2.4'
    });
  };

</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.4&appId=1789977611070117&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<!-- <div class="fb-login-button" data-max-rows="1" data-size="medium" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="true" data-use-continue-as="true"></div> --> 
<br>
<button   id="login"  class="btn btn-primary" onclick="login()">Sign in facebook</button><br>
<br>
<button  id="Logout"  style="display:none;" class="btn btn-primary" onclick="logout()">Sign Out from facebook</button><br>


   <button id = "signout" class="btn btn-primary" onclick="myFunction()">Sign Out from Gmail</button>
   
   <script>
      function myFunction() {
      gapi.auth2.getAuthInstance().disconnect();
       location.reload();
   			}
      
      function logout()
      {FB.logout(function(response) {
    	  // user is now logged out
    	 debugger
    	  document.getElementById("name").innerHTML = '';
    	  document.getElementById("status").innerHTML  = '';
    	  
    	  document.getElementById("login").style.display = "block";
	       document.getElementById("Logout").style.display = "none";
    	});
      }
      
      
      function login()
      {
    	 /*  FB.login(function(response) {
    		  debugger
    		  // handle the response
    		}, {scope: 'public_profile,email'}); */
    	  FB.login(function(response) {
    		  debugger
    		    if (response.authResponse) {
    		     console.log('Welcome!  Fetching your information.... ');
    		     FB.api('/me', function(response) {
    		       console.log('Good to see you, ' + response.name + '.');
    		      
    		       document.getElementById("name").innerHTML = response.name;
    		       document.getElementById("status").innerHTML = 'Welcome '+response.name+'!<a href=static/pages/home.jsp?email='+response.id+'&name='+ response.name+'/>Continue with FaceBook login</a></p>'
    		       document.getElementById("login").style.display = "none";
    		       document.getElementById("Logout").style.display = "block";
    		     });
    		    } else {
    		     console.log('User cancelled login or did not fully authorize.');
    		    }
    		});

      }
   </script>
</center></body>
</html>
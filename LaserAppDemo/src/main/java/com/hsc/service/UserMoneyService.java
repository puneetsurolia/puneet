package com.hsc.service;

import com.hsc.model.UserMoneyForm;

public interface UserMoneyService {
	String addUserMoney(UserMoneyForm userMoneyForm);
}

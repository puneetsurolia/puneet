package com.hsc.service;

import com.hsc.dao.UserMoneyDao;
import com.hsc.model.UserMoneyForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserMoneyServiceImpl implements UserMoneyService{
	
	@Autowired
	UserMoneyDao userMoneyDao;
	
	public String addUserMoney(UserMoneyForm userMoneyForm) {
		String str=userMoneyDao.addUserMoney(userMoneyForm);
		return str;
	}
}

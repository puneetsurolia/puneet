package com.hsc.dao;

import com.hsc.model.UserMoneyForm;

public interface UserMoneyDao {
	String addUserMoney(UserMoneyForm userMoneyForm);
}

package com.hsc.dao;

import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.transaction.Transactional;

import com.hsc.entity.UserMoneyEntity;
import com.hsc.model.UserMoneyForm;
import com.hsc.script.RemoteScriptExec;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class UserMoneyDaoImpl implements UserMoneyDao {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	ServletContext context;
	
	
	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	/*@Autowired
	ServletContext context;*/
	@Override
	public String addUserMoney(UserMoneyForm userMoneyForm) {
		System.out.println("in dao------------------");
		UserMoneyEntity userMoneyEntity = new UserMoneyEntity();

		userMoneyEntity.setUsername(userMoneyForm.getUsername());
		userMoneyEntity.setUsermoney(Integer.parseInt(userMoneyForm.getUsermoney()));
		userMoneyEntity.setInsertdate(new Date());

		
		int entredmoney = (userMoneyEntity.getUsermoney());
		
		Query query = sessionFactory.getCurrentSession().createQuery("from UserMoneyEntity where username=:username AND date(insertdate) = curdate()");
		query.setParameter("username", userMoneyForm.getUsername());
		List<UserMoneyEntity> userMoneyList = (List<UserMoneyEntity>) query.list();

		System.out.println(userMoneyList.size());

		int totalAmt = 0;
		for (int i = 0; i < userMoneyList.size(); i++) {
			totalAmt = totalAmt +(userMoneyList.get(i).getUsermoney());
		}

		totalAmt = totalAmt+ Integer.parseInt(userMoneyForm.getUsermoney());
		
		if (totalAmt <= 100 && entredmoney <= 100 ){
			if(RemoteScriptExec.callScript())
			{
			getSession().save(userMoneyEntity);
			context.setAttribute("totalAmount", totalAmt);
			return "success";
			}else
			{
				return "errorpage";
			}
		} else {
			return "errorpage";
		}
		
		
	}
}

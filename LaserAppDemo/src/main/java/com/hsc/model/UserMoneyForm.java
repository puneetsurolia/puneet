package com.hsc.model;

public class UserMoneyForm {
	private String username;
	private String usermoney;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsermoney() {
		return usermoney;
	}
	public void setUsermoney(String usermoney) {
		this.usermoney = usermoney;
	}
	
	
	
}

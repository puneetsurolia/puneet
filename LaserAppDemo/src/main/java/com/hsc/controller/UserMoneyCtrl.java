package com.hsc.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.hsc.model.UserMoneyForm;
import com.hsc.script.VerifyRecaptcha;
import com.hsc.service.UserMoneyService;

@RestController
public class UserMoneyCtrl {
	
	   
	@Autowired
	UserMoneyService userMoneyService;
	
	@RequestMapping(value = "/add/usermoney", method = RequestMethod.POST)
	public  ModelAndView addUserMoney(HttpServletRequest request, HttpServletResponse response,
			  @ModelAttribute("LaserAppDemo") UserMoneyForm userMoneyForm) throws IOException {
		ModelAndView model = new ModelAndView();
		
		// get request parameters for userID and password
				String user = request.getParameter("username");
				String amt = request.getParameter("usermoney");
		
		// get reCAPTCHA request param
				String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
				System.out.println(gRecaptchaResponse);
				boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);
		
				if (user != null && amt != null && verify) {
					//if (user != null && amt != null){
						if(userMoneyService.addUserMoney(userMoneyForm).equals("success")) {
						model.setViewName("success");
					}else {
						model.setViewName("errorpage");
					}
				} else {
					
					model.setViewName("invalidCaptcha");
				
				}
	
		return model;
	}

	










}

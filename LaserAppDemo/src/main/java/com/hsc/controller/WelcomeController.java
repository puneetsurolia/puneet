package com.hsc.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/")
public class WelcomeController {
	
	@RequestMapping(method = RequestMethod.GET)
    public ModelAndView getWelcomePage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("index");
		return model;
    }
	
	
    
}
